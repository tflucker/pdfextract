package com.tim;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSFloat;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class App {

    public static void main(final String[] args) {
        System.out.println("-------- Initiating process --------\n");

        System.out.println("Reading from: " + args[0]);
        System.out.println("Saving result files in: " + args[1]);
        final long start = System.currentTimeMillis();
        try {
            final File folder = new File(args[0]);
            for (final File file : folder.listFiles()) {
                readPDF(file.getAbsolutePath(), file.getName(), args[1]);
            }
            System.out.println("Total Files processed: " + folder.listFiles().length);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        final long end = System.currentTimeMillis();
        System.out.println("Total Processing Time: " + ((end - start) / 1000) + " seconds");

        System.out.println("-------- Terminating process -------- \n");

    }

    public static void readPDF(final String filePath, final String filename, final String resultPath)
            throws IOException {
        final Map<String, String> annotationTextMap = new HashMap<>();
        final List<String> resultStrings = new ArrayList<>();
        final List<String> sentences = new ArrayList<>();

        try {
            // instatiate pdf document based on file input
            final PDDocument doc = PDDocument.load(new File(filePath));
            final PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);

            // Strip text from pdf
            final PDFTextStripper tStripper = new PDFTextStripper();
            final String pdfFileText = tStripper.getText(doc);

            // split pdf text into sentences based on periods
            final BreakIterator iter = BreakIterator.getSentenceInstance(Locale.US);
            iter.setText(pdfFileText);
            int start = 0;
            int end = 0;
            while ((end = iter.next()) != BreakIterator.DONE) {
                final String sentence = pdfFileText.substring(start, end);
                sentences.add(sentence);
                start = end;
            }
            // removes all sentences that are just '.'
            sentences.removeIf(s -> StringUtils.equals(s, "."));

            // using pdf document, get all annotations
            final PDPageTree allPages = doc.getDocumentCatalog().getPages();
            for (final PDPage page : allPages) {
                // get annotation dictionaries
                final List<PDAnnotation> annotations = page.getAnnotations();

                // sorts annotations based on position in the page (90% accurate)
                final Comparator<PDAnnotation> comp = (final PDAnnotation pd1, final PDAnnotation pd2) -> Float
                        .valueOf(pd1.getRectangle().getHeight())
                        .compareTo(Float.valueOf(pd2.getRectangle().getHeight()));
                Collections.sort(annotations, comp);

                for (int i = 0; i < annotations.size(); i++) {
                    // check subType
                    final PDAnnotation annotation = annotations.get(i);
                    if(annotation.getContents() == null){
                        break;
                    }
                    else if (annotation.getSubtype().equals("Highlight")) {
                        // extract highlighted text
                        final PDFTextStripperByArea stripperByArea = new PDFTextStripperByArea();

                        final COSArray quadsArray = (COSArray) annotation.getCOSObject()
                                .getDictionaryObject(COSName.getPDFName("QuadPoints"));
                        String str = null;

                        for (int j = 1, k = 0; j <= (quadsArray.size() / 8); j++) {

                            final COSFloat ULX = (COSFloat) quadsArray.get(0 + k);
                            final COSFloat ULY = (COSFloat) quadsArray.get(1 + k);
                            final COSFloat URX = (COSFloat) quadsArray.get(2 + k);
                            final COSFloat URY = (COSFloat) quadsArray.get(3 + k);
                            final COSFloat LLX = (COSFloat) quadsArray.get(4 + k);
                            final COSFloat LLY = (COSFloat) quadsArray.get(5 + k);

                            k += 8;

                            final float ulx = ULX.floatValue() - 1; // upper left x.
                            float uly = ULY.floatValue(); // upper left y.
                            final float width = URX.floatValue() - LLX.floatValue(); // calculated by upperRightX -
                            // lowerLeftX.
                            final float height = URY.floatValue() - LLY.floatValue(); // calculated by upperRightY -
                            // lowerLeftY.

                            final PDRectangle pageSize = page.getMediaBox();
                            uly = pageSize.getHeight() - uly;

                            final Rectangle2D rectangle_2 = new Rectangle2D.Float(ulx, uly, width, height);
                            stripperByArea.addRegion("highlightedRegion", rectangle_2);
                            stripperByArea.extractRegions(page);
                            String highlightedText = stripperByArea.getTextForRegion("highlightedRegion");
                            highlightedText = highlightedText.replaceAll("\r\n", " ").replaceAll("\\$\\s", "\\$");

                            if (j > 1) {
                                // regex to clean new lines into spaces
                                str = str.concat(highlightedText);
                            } else {
                                str = highlightedText;
                            }
                        }
                        annotationTextMap.put(str.trim().replaceAll("- ", ""), annotation.getContents());
                    }
                }
            }

            // go though each line in doc, and check for match with annotation
            for (String line : sentences) {
                line = line.replaceAll("\r\n", " ").replaceAll("- ", "").trim();
                boolean matchFound = false;

                // check to see if line contains any of the highlighted text, if so then
                // determine which one and its indicies
                for (String key : annotationTextMap.keySet()) {
                    int startIndex = 0;
                    int endIndex = 0;
                    key = key.trim();
                    String value = annotationTextMap.get(key);
                    value = value.replaceAll("\"", "").replaceAll("\'", "");
                    if (line.contains(key)) {
                        // if new line chars found, break up annotations
                        if (value.contains("\r") || value.contains("\n")) {
                            String[] annotationTexts = value.replaceAll("\r", "\n").split("\n");
                            for (String aText : annotationTexts) {
                                if (aText.contains(" - ")) {
                                    String[] parts = aText.split(" - ");
                                    value = parts[0];
                                    key = parts[1].replaceAll("\"", "").replaceAll("\'", "");
                                }
                                startIndex = line.indexOf(key);
                                endIndex = startIndex + key.length();
                                resultStrings.add("(\"" + line + "\", {\"entities\":[(" + String.valueOf(startIndex)
                                        + "," + String.valueOf(endIndex) + ",\"" + value + "\")]}),\n");
                            }
                        } else {
                            startIndex = line.indexOf(key);
                            endIndex = startIndex + key.length();
                            resultStrings.add("(\"" + line + "\", {\"entities\":[(" + String.valueOf(startIndex) + ","
                                    + String.valueOf(endIndex) + ",\"" + value + "\")]}),\n");
                        }
                        matchFound = true;
                    } else if (key.matches("\\.{2}") && (key.contains(line))
                            || key.contains(line.substring(0, (int) Math.ceil(line.length() / 4)))) {
                        if (value.contains("\r") || value.contains("\n")) {
                            String[] annotationTexts = value.replaceAll("\r", "\n").split("\n");
                            for (String aText : annotationTexts) {
                                startIndex = 0;
                                endIndex = line.length() - 1;
                                if (aText.contains(" - ")) {
                                    String[] parts = aText.split(" - ");
                                    value = parts[0];
                                    key = parts[1].replaceAll("\"", "").replaceAll("\'", "");
                                    if (line.contains(key)) {
                                        startIndex = line.indexOf(key);
                                        endIndex = startIndex + key.length();
                                    }
                                }
                                resultStrings.add("(\"" + line + "\", {\"entities\":[(" + String.valueOf(startIndex)
                                        + "," + String.valueOf(endIndex) + ",\"" + value + "\")]}),\n");
                            }
                        } else {
                            startIndex = 0;
                            endIndex = line.length() - 1;
                            resultStrings.add("(\"" + line + "\", {\"entities\":[(" + String.valueOf(startIndex) + ","
                                    + String.valueOf(endIndex) + ",\"" + value + "\")]}),\n");
                        }
                        matchFound = true;
                    }
                }
                // if no match is found, return default result string
                if (!matchFound) {
                    resultStrings.add("(\"" + line + "\", {\"entities\":[(0,0,\"\")]}),\n");
                }
            }

            // close document to prevent memory leak
            doc.close();

            // write all data into .xlsx excel file
            writeToExcel(resultStrings, resultPath + createResultFilename(filename, "xlsx"));

        } catch (final Exception e) {
            System.out.println("Filename: " + filename);
            e.printStackTrace();
        }
    }

    // given the input file name, create result filename string using the same name
    // with provided extension
    public static String createResultFilename(final String input, final String extension) {
        String result = "output-result.txt";

        final String[] inputParts = StringUtils.split(input, "/");
        for (final String part : inputParts) {
            if (part.contains(".")) {
                result = StringUtils.split(part, ".")[0] + "-result." + extension;
            }
        }
        return result;
    }

    // create xlsx file with provided data at specified location
    public static void writeToExcel(final List<String> data, final String resultFile) throws IOException {
        final Workbook workbook = new XSSFWorkbook();

        final Sheet sheet = workbook.createSheet("Extracted Data");

        int rowNum = 0;

        // add data to document
        for (final String d : data) {
            final Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(d);
        }

        // auto size column
        sheet.autoSizeColumn(0);

        // create actual file based on workbook object
        final FileOutputStream fOut = new FileOutputStream(resultFile);

        // close all streams to prevent memory leaks
        workbook.write(fOut);
        fOut.close();
        workbook.close();
    }

    /* ---------------------- OLD METHODS ---------------------- */

    // PDF32000-2008
    // 12.5.2 Annotation Dictionaries
    // 12.5.6 Annotation Types
    // 12.5.6.10 Text Markup Annotations
    @SuppressWarnings({ "unused" })
    public static List<String> getHighlightedText(final String filePath) throws IOException {
        final List<String> highlightedTexts = new ArrayList<>();
        // this is the in-memory representation of the PDF document.
        // this will load a document from a file.
        final PDDocument document = PDDocument.load(new File(filePath));
        // this represents all pages in a PDF document.
        final PDPageTree allPages = document.getDocumentCatalog().getPages();
        int pageNum = 0;
        for (final PDPage page : allPages) {
            pageNum++;
            highlightedTexts.add("\n ----- Page: " + pageNum + " ----- \n");
            // get annotation dictionaries
            final List<PDAnnotation> annotations = page.getAnnotations();

            // sorts annotations based on position in the page (90% accurate)
            final Comparator<PDAnnotation> comp = (final PDAnnotation pd1, final PDAnnotation pd2) -> Float
                    .valueOf(pd1.getRectangle().getHeight()).compareTo(Float.valueOf(pd2.getRectangle().getHeight()));
            Collections.sort(annotations, comp);

            for (int i = 0; i < annotations.size(); i++) {
                // check subType
                final PDAnnotation annotation = annotations.get(i);

                if (annotation.getSubtype().equals("Highlight")) {
                    // extract highlighted text
                    final PDFTextStripperByArea stripperByArea = new PDFTextStripperByArea();

                    final COSArray quadsArray = (COSArray) annotation.getCOSObject()
                            .getDictionaryObject(COSName.getPDFName("QuadPoints"));
                    String str = null;

                    for (int j = 1, k = 0; j <= (quadsArray.size() / 8); j++) {

                        final COSFloat ULX = (COSFloat) quadsArray.get(0 + k);
                        final COSFloat ULY = (COSFloat) quadsArray.get(1 + k);
                        final COSFloat URX = (COSFloat) quadsArray.get(2 + k);
                        final COSFloat URY = (COSFloat) quadsArray.get(3 + k);
                        final COSFloat LLX = (COSFloat) quadsArray.get(4 + k);
                        final COSFloat LLY = (COSFloat) quadsArray.get(5 + k);
                        final COSFloat LRX = (COSFloat) quadsArray.get(6 + k);
                        final COSFloat LRY = (COSFloat) quadsArray.get(7 + k);

                        k += 8;

                        final float ulx = ULX.floatValue() - 1; // upper left x.
                        float uly = ULY.floatValue(); // upper left y.
                        final float width = URX.floatValue() - LLX.floatValue(); // calculated by upperRightX -
                                                                                 // lowerLeftX.
                        final float height = URY.floatValue() - LLY.floatValue(); // calculated by upperRightY -
                                                                                  // lowerLeftY.

                        final PDRectangle pageSize = page.getMediaBox();
                        uly = pageSize.getHeight() - uly;

                        final Rectangle2D rectangle_2 = new Rectangle2D.Float(ulx, uly, width, height);
                        stripperByArea.addRegion("highlightedRegion", rectangle_2);
                        stripperByArea.extractRegions(page);
                        final String highlightedText = stripperByArea.getTextForRegion("highlightedRegion");
                        highlightedText.replaceAll("\n", " ");

                        if (j > 1) {
                            // regex to clean new lines into spaces
                            str = str.concat(highlightedText);
                        } else {
                            str = highlightedText;
                        }
                    }
                    highlightedTexts.add('(' + annotation.getContents() + ')');
                    highlightedTexts.add(str);
                }
            }
        }
        document.close();

        return highlightedTexts;
    }

    // PDF32000-2008
    // 12.5.2 Annotation Dictionaries
    // 12.5.6 Annotation Types
    // 12.5.6.10 Text Markup Annotations
    @SuppressWarnings({ "unused" })
    public static List<String> getHighlightedText2(final String filePath) throws IOException {
        final List<String> highlightedTexts = new ArrayList<>();
        // this is the in-memory representation of the PDF document.
        // this will load a document from a file.
        final PDDocument document = PDDocument.load(new File(filePath));
        // this represents all pages in a PDF document.
        final PDPageTree allPages = document.getDocumentCatalog().getPages();
        int pageNum = 0;
        for (final PDPage page : allPages) {
            pageNum++;
            // get annotation dictionaries
            final List<PDAnnotation> annotations = page.getAnnotations();

            // sorts annotations based on position in the page (90% accurate)
            final Comparator<PDAnnotation> comp = (final PDAnnotation pd1, final PDAnnotation pd2) -> Float
                    .valueOf(pd1.getRectangle().getHeight()).compareTo(Float.valueOf(pd2.getRectangle().getHeight()));
            Collections.sort(annotations, comp);

            for (int i = 0; i < annotations.size(); i++) {
                // check subType
                final PDAnnotation annotation = annotations.get(i);

                if (annotation.getSubtype().equals("Highlight")) {
                    // extract highlighted text
                    final PDFTextStripperByArea stripperByArea = new PDFTextStripperByArea();

                    final COSArray quadsArray = (COSArray) annotation.getCOSObject()
                            .getDictionaryObject(COSName.getPDFName("QuadPoints"));
                    String str = null;

                    for (int j = 1, k = 0; j <= (quadsArray.size() / 8); j++) {

                        final COSFloat ULX = (COSFloat) quadsArray.get(0 + k);
                        final COSFloat ULY = (COSFloat) quadsArray.get(1 + k);
                        final COSFloat URX = (COSFloat) quadsArray.get(2 + k);
                        final COSFloat URY = (COSFloat) quadsArray.get(3 + k);
                        final COSFloat LLX = (COSFloat) quadsArray.get(4 + k);
                        final COSFloat LLY = (COSFloat) quadsArray.get(5 + k);
                        final COSFloat LRX = (COSFloat) quadsArray.get(6 + k);
                        final COSFloat LRY = (COSFloat) quadsArray.get(7 + k);

                        k += 8;

                        final float ulx = ULX.floatValue() - 1; // upper left x.
                        float uly = ULY.floatValue(); // upper left y.
                        final float width = URX.floatValue() - LLX.floatValue(); // calculated by upperRightX -
                                                                                 // lowerLeftX.
                        final float height = URY.floatValue() - LLY.floatValue(); // calculated by upperRightY -
                                                                                  // lowerLeftY.

                        final PDRectangle pageSize = page.getMediaBox();
                        uly = pageSize.getHeight() - uly;

                        final Rectangle2D rectangle_2 = new Rectangle2D.Float(ulx, uly, width, height);
                        stripperByArea.addRegion("highlightedRegion", rectangle_2);
                        stripperByArea.extractRegions(page);
                        String highlightedText = stripperByArea.getTextForRegion("highlightedRegion");
                        // remove new line characters and carriage returns
                        highlightedText = highlightedText.replaceAll("\\r\\n", "");

                        if (j > 1) {
                            str = str.concat(highlightedText);
                        } else {
                            str = highlightedText;
                        }
                    }

                    // using | separator because highlighted text might contain commas and we don't
                    // want any data loss
                    highlightedTexts.add(String.valueOf(pageNum) + "|" + annotation.getContents() + "|" + str);
                }
            }
        }
        document.close();

        return highlightedTexts;
    }

}
